﻿// Homework175.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>


class Human
{
public:
    Human()
    {
        m_Name = "Ivan";
        m_SurName = "Ivanov";
        m_Age = 33;
    }
    
    Human(std::string name, std::string surName, int age)
        : m_Name(name), m_SurName(surName), m_Age(age)
    {}

    void printInfo()
    {
        std::cout << m_Name << ' ' << m_SurName << " is " << m_Age << " years old\n";
    }

private:
    std::string m_Name, m_SurName;
    int m_Age;

};


class Vector
{
public:
    Vector() : m_X(2), m_Y(2), m_Z(2)
    {}

    Vector(double x, double y, double z) : m_X(x), m_Y(y), m_Z(z)
    {}

    void show()
    {
        std::cout << m_X << ' ' << m_Y << ' ' << m_Z << '\n';
    }

    double getLength()
    {
        double length = sqrt(m_X * m_X + m_Y * m_Y + m_Z * m_Z);
        return length;
    }

private:
    double m_X;
    double m_Y;
    double m_Z;

};

int main()
{
    Vector v1;
    v1.show();
    std::cout << "Length of vector 1 is " << v1.getLength() << '\n';

    Vector v2(2, -4, 4);
    v2.show();
    std::cout << "Length of vector 2 is " << v2.getLength() << '\n';
    
    std::cout << "\n\n";

    Human h1;
    h1.printInfo();

    Human h2("Harry", "Potter", 15);
    h2.printInfo();


}

